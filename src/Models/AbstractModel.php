<?php

/**
 * @class AbstractModel
 * @package PlannerClient\Models
 */

namespace PlannerClient\Models;

use ReflectionClass;
use ReflectionProperty;

abstract class AbstractModel
{
    /**
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        $props = $this->getProperties();

        foreach($props as $prop)
        {
            $getter = 'get' . ucfirst($prop);

            if(method_exists($this, $getter))
            {
                if($this->$getter())
                {
                    if(method_exists($this->$getter(), 'toArray'))
                    {
                        $result[$prop] = $this->$getter()->toArray();
                    }
                    else
                    {
                        $result[$prop] = $this->$getter();
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param array $array
     * @return this
     */
    function fromArray(?array $array = [])
    {
        foreach ($array as $key => $value) {
            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    private function getProperties(): array
    {
        $props = [];
        $reflect = new ReflectionClass($this);
        $reflect = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED |  ReflectionProperty::IS_PRIVATE);

        foreach($reflect as $prop)
        {
            $props[] = $prop->getName();
        }

        return $props;
    }
}
