<?php
/**
 * @class StatusesScope
 * @package PlannerClient\Models\Scopes
 */

namespace PlannerClient\Models\Scopes;

use PlannerClient\Models\Interfaces\StatusesScopeInterface;
use PlannerClient\Models\Scopes\BaseScope;
use PlannerClient\Models\Scopes\ScopesValues\StatusesScopeValue;

class StatusesScope extends BaseScope implements StatusesScopeInterface
{
    /**
     * @var string
     */
    public $name = self::NAME;

    /**
     * @var mixed
     */
    public $value;

    /**
     * @param StatusesScopeValue $value
     * @return this
     */
    public function setValue($value): self
    {
        $this->value = (new StatusesScopeValue())->fromArray($value);
        return $this;
    }

    /**
     * StatusesScope constructor
     */
    public function __construct($value = [])
    {
        $this->setValue($value);
    }
}