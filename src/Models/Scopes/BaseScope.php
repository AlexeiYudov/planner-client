<?php
/**
 * @class BaseScope
 * @package PlannerClient\Models\Scopes
 */

namespace PlannerClient\Models\Scopes;

use PlannerClient\Models\AbstractModel;

abstract class BaseScope extends AbstractModel
{
    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return this
     */
    public function setValue($value): self
    {
        $this->value = $value;
        return $this;
    }
}