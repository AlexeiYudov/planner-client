<?php
/**
 * @class UsersScope
 * @package PlannerClient\Models\Scopes
 */

namespace PlannerClient\Models\Scopes;

use PlannerClient\Models\Interfaces\UsersScopeInterface;
use PlannerClient\Models\Scopes\BaseScope;
use PlannerClient\Models\Scopes\ScopesValues\UsersScopeValue;

class UsersScope extends BaseScope implements UsersScopeInterface
{
    /**
     * @var string
     */
    private $name = self::NAME;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @param UsersScopeValue $value
     * @return this
     */
    public function setValue($value): self
    {
        $this->value = (new UsersScopeValue())->fromArray($value);
        return $this;
    }

    /**
     * UsersScope constructor
     */
    public function __construct($value = [])
    {
        $this->setValue($value);
    }
}