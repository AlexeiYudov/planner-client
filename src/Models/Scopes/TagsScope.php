<?php
/**
 * @class TagsScope
 * @package PlannerClient\Models\Scopes
 */

namespace PlannerClient\Models\Scopes;

use PlannerClient\Models\Interfaces\TagsScopeInterface;
use PlannerClient\Models\Scopes\BaseScope;
use PlannerClient\Models\Scopes\ScopesValues\TagsScopeValue;

class TagsScope extends BaseScope implements TagsScopeInterface
{
    /**
     * @var string
     */
    public $name = self::NAME;

    /**
     * @var mixed
     */
    public $value;

    /**
     * @param TagsScopeValue $value
     * @return this
     */
    public function setValue($value): self
    {
        $this->value = (new TagsScopeValue())->fromArray($value);
        return $this;
    }

    /**
     * TagsScope constructor
     */
    public function __construct($value = [])
    {
        $this->setValue($value);
    }
}