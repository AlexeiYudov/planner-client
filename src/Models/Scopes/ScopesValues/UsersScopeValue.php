<?php
/**
 * @class UsersScopeValue
 * @package PlannerClient\Models\Scopes\ScopesValues
 */

namespace PlannerClient\Models\Scopes\ScopesValues;

use PlannerClient\Models\AbstractModel;

class UsersScopeValue extends AbstractModel
{
    /**
     * @var array
     */
    private $value;

    /**
     * @return array
     */
    public function getValue(): ?array
    {
        return $this->value;
    }

    /**
     * @param array $array
     * @return this
     */
    public function setValue(array $value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * UsersScopeValue contructor
     */
    public function __construct(array $value = [])
    {
        $this->value = $value;
    }
}