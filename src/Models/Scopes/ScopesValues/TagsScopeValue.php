<?php
/**
 * @class TagsScopeValue
 * @package PlannerClient\Models\Scopes\ScopesValues
 */

namespace PlannerClient\Models\Scopes\ScopesValues;

use PlannerClient\Models\AbstractModel;

class TagsScopeValue extends AbstractModel
{
    /**
     * @var array
     */
    private $value;

    /**
     * @return array
     */
    public function getValue(): ?array
    {
        return $this->value;
    }

    /**
     * @param array $array
     * @return this
     */
    public function setValue(array $value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * TagsScopeValue contructor
     */
    public function __construct(array $value = [])
    {
        $this->value = $value;
    }
}