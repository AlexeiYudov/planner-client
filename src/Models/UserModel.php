<?php
/**
 * @class UserModel
 * @package PlannerClient\Models
 */

namespace PlannerClient\Models;

use PlannerClient\Models\AbstractModel;

class UserModel extends AbstractModel
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $id;

    /**
     * @var UsersGroup
     */
    private $group;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return UsersGroup
     */
    public function getGroup(): ?UsersGroup
    {
        return $this->group;
    }

    /**
     * @param UsersGroup $group
     * @return this
     */
    public function setGroup(UsersGroup $group): self
    {
        $this->group = $group;
        return $this;
    }
}