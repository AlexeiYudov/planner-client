<?php
/**
 * @class ResponsibleUsers
 * @extends AbstractModel
 * @package PlannerClient\Models
 */

namespace PlannerClient\Models;

use PlannerClient\Collections\UsersCollection;
use PlannerClient\Collections\UsersGroupsCollection;
use PlannerClient\Exceptions\PlannerClientInvalidValueException;
use PlannerClient\Models\AbstractModel;
use PlannerClient\Models\Interfaces\ResponsibleManagersTypes;

class ResponsibleUsers extends AbstractModel
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var UsersGroupsCollection|UsersCollection
     */
    private $value;

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return this
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|UsersGroupsCollectionsCollection|UsersCollection
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param UsersGroupsCollection|UsersCollection
     * @return this
     */
    public function setValue($value): ?self
    {
        try
        {
            if($this->validateValue($value))
            {
                $this->value = $value;
            }
        }
        catch(PlannerClientInvalidValueException $e)
        {
            print_r($e->getMessage());
            return null;
        }

        return $this;
    }

    /**
     * ResponsibleUsers constructor
     */
    public function __construct(string $type)
    {
        $this->setType($type);

        if($type === ResponsibleManagersTypes::TYPE_USERS_GROUPS_NAME)
        {
            $this->setValue(new UsersGroupsCollection());
        }

        if($type === ResponsibleManagersTypes::TYPE_USERS_NAME)
        {
            $this->setValue(new UsersCollection());
        }
    }

    /**
     * @param UsersGroupsCollection|UsersCollection $value
     * @return bool
     */
    private function validateValue($value): bool
    {
        if(gettype($value) === 'object')
        {
            if(get_class($value) === UsersGroupsCollection::class || get_class($value) === UsersCollection::class)
            {
                return true;
            }
            else
            {
                throw new PlannerClientInvalidValueException(
                    'Expected type of value property: '
                    . 'UsersGroupsCollection or UsersCollection. '
                    . 'Received invalid ' . gettype($value) . ' type.'
                );
            }
        }
        else
        {
            throw new PlannerClientInvalidValueException(
                'Expected type of value property: '
                . 'UsersGroupsCollection or UsersCollection. '
                . 'Received invalid ' . gettype($value) . ' type.'
            );
        }

        return false;
    }
}