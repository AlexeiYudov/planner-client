<?php
/**
 * @class TagsScopeInterface
 * @package PlannerClient\Models\Interfaces
 */

namespace PlannerClient\Models\Interfaces;

use PlannerClient\Models\Scopes\ScopesValues\TagsScopeValue;

interface TagsScopeInterface
{
    public const NAME = 'tags-scope';

    public const VALUE_MODEL = TagsScopeValue::class;
}