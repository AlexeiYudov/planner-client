<?php
/**
 * @class UsersScopeInterface
 * @package PlannerClient\Models\Interfaces
 */

namespace PlannerClient\Models\Interfaces;

use PlannerClient\Models\Scopes\ScopesValues\UsersScopeValue;

interface UsersScopeInterface
{
    public const NAME = 'users-scope';

    public const VALUE_MODEL = UsersScopeValue::class;
}