<?php
/**
 * @class StatusesScopeInterface
 * @package PlannerClient\Models\Interfaces
 */

namespace PlannerClient\Models\Interfaces;

use PlannerClient\Models\Scopes\ScopesValues\StatusesScopeValue;

interface StatusesScopeInterface
{
    public const NAME = 'statuses-scope';

    public const VALUE_MODEL = StatusesScopeValue::class;
}