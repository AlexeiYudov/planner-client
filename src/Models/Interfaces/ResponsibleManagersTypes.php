<?php
/**
 * @class ResponsibleManagersTypes
 * @package PlannerClient\Models\Interfaces
 */

namespace PlannerClient\Models\Interfaces;

use PlannerClient\Collections\UsersGroupsCollection;
use PlannerClient\Collections\UsersCollection;

interface ResponsibleManagersTypes
{
    /**
     * @var UsersGroupsCollection
     */
    public const TYPE_USERS_GROUPS = UsersGroupsCollection::class;

    /**
     * @var string
     */
    public const TYPE_USERS_GROUPS_NAME = 'users-groups';

    /**
     * @var UsersCollection
     */
    public const TYPE_USERS = UsersCollection::class;

    /**
     * @var string
     */
    public const TYPE_USERS_NAME = 'users';
}