<?php
/**
 * @class UsersGroup
 * @package PlannerClient\Models
 */

namespace PlannerClient\Models;

use PlannerClient\Models\AbstractModel;

class UsersGroup extends AbstractModel
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $id;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }
}