<?php
/**
 * @class PlannerClientInvalidValueException
 * @package PlannerClient\Exceptions
 */

namespace PlannerClient\Exceptions;

use PlannerClient\Exceptions\PlannerClientException;

class PlannerClientInvalidValueException extends PlannerClientException
{
    //
}