<?php
/**
 * @class ScopesCollection
 * @package PlannerClient\Collections
 */

namespace PlannerClient\Collections;

use PlannerClient\Collections\AbstractCollection;
use PlannerClient\Models\Scopes\BaseScope;

class ScopesCollection extends AbstractCollection
{
    /**
     * @var BaseScope
     */
    public $_MODEL = BaseScope::class;

    /**
     * @return null
     */
    public function fromArray(array $array = []): ?self
    {
        return null;
    }
}