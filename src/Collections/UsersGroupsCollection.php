<?php
/**
 * @class UsersGroupsCollection
 * @package PlannerClient\Collections
 */

namespace PlannerClient\Collections;

use PlannerClient\Collections\AbstractCollection;
use PlannerClient\Models\UsersGroup;

class UsersGroupsCollection extends AbstractCollection
{
    /**
     * @var UsersGroup
     */
    public $_MODEL = UsersGroup::class;
}