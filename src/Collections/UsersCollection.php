<?php
/**
 * @class UsersCollection
 * @package PlannerClient\Collections
 */

namespace PlannerClient\Collections;

use PlannerClient\Collections\AbstractCollection;
use PlannerClient\Models\UserModel;

class UsersCollection extends AbstractCollection
{
    /**
     * @var UserModel
     */
    public $_MODEL = UserModel::class;
}