<?php

namespace PlannerClient\Collections;


abstract class AbstractCollection
{
    /**
     * @var object
     */
    public $_MODEL = null;

    /**
     * @var array
     */
    protected $items;

    /**
     * @param array $items
     * @return this
     */
    public function __construct(?array $items = [])
    {
        $this->items = $items;
    }

    /**
     * @return AbstractModel|array
     */
    public function first()
    {
        return (isset($this->items[0]))
            ? $this->items[0]
            : $this->items;
    }

    /**
     * @return AbstractModel|array
     */
    public function last()
    {
        return (isset($this->items[count($this->items) - 1]))
            ? $this->items[count($this->items) - 1]
            : $this->items;
    }

    /**
     * @param array
     * @return this
     */
    public function add(array $models): self
    {
        $this->items = array_merge($this->items, $models);
        return $this;
    }

    /**
     * @param AbstractModel
     * @return this
     */
    public function addOne($model): self
    {
        array_push($this->items, $model);
        return $this;
    }

    /**
     * @param AbstractModel
     * @return this
     */
    public function removeModel($model): self
    {
        foreach ($this->items as $index => $item) {
            if ($item === $model) {
                unset($this->items[$index]);
            }
        }

        return $this;
    }

    /**
     * @param integer
     * @return this
     */
    public function remove($index): self
    {
        if (isset($this->items[$index])) {
            unset($this->items[$index]);
        }

        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return AbstractModel|boolean
     */
    public function findBy(?string $key, $value)
    {
        foreach ($this->items as $item) {
            $method = 'get' . ucfirst($key);

            if (method_exists($item, $method)) {
                if ($item->$method() == $value) {
                    return $item;
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function items(): array
    {
        return $this->items;
    }

    /**
     * @param array $array
     * @return this
     */
    public function fromArray(array $array = []): ?self
    {
        foreach ($array as $model) {
            $this->addOne(
                (new $this->_MODEL())->fromArray($model)
            );
        }

        return $this;
    }
}
